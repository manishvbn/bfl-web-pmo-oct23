"use strict";

require("./check.js");
console.log('Hello from the main file');
const obj = {
  arr: [1, 2, 3, 4, 5],
  printArr: function () {
    console.log(...this.arr);
  },
  find: function (dataToFind) {
    return this.arr.find(item => item === dataToFind);
  }
};
obj.printArr();
console.log(obj.find(10));
check();