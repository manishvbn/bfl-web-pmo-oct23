const HtmlWebpackPlugin = require("html-webpack-plugin");
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
const chalk = require('chalk');

module.exports = function (env) {
    // Webpack Configuration Object
    return {
        entry: {
            app: './src/main'
        },

        resolve: {
            extensions: [".js"]
        },

        module: {
            rules: [
                {
                    test: /\.(js)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.html$/,
                    use: "html-loader",
                },
                {
                    test: /\.(png|svg|jpg|jpeg|gif)$/i,
                    type: "asset",
                    parser: {
                        dataUrlCondition: {
                            maxSize: 50 * 1024,
                        },
                    },
                    generator: {
                        filename: "images/[hash][name][ext]",
                    },
                },
                {
                    test: /\.css$/,
                    use: [
                        process.env.NODE_ENV === "production" ? MiniCssExtractPlugin.loader : "style-loader",
                        "css-loader",
                        {
                            loader: "postcss-loader",
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        ["postcss-preset-env", {}]
                                    ]
                                }
                            }
                        }
                    ]
                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                template: "./public/index.html",   // Input FileName
                filename: "./index.html",    // Output FileName
                scriptLoading: 'blocking',
                favicon: "./public/js-favicon.png"
            }),
            new ProgressBarPlugin({
                format: '  build [:bar] ' + chalk.green.bold(':percent') + '\t' + chalk.blue.bold(':elapsed seconds'),
                clear: false
            })
        ],

        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        }
    };
};