// Syntax One - Default Import

// import square from "./lib";
// console.log("Square: ", square(20));

// import sqr from "./lib";
// console.log("Square: ", sqr(20));

// import * as l from "./lib";
// console.log("Square: ", l.default(20));

// Syntax Two - Named Import
// import { square } from "./lib";
// console.log("Square: ", square(20));

// import { square as sqr } from "./lib";
// console.log("Square: ", sqr(20));

// import * as l from "./lib";
// console.log("Square: ", l.square(20));

// Syntax Three - Multi Import
// import square, { check, test } from "./lib";
// console.log("Square: ", square(5));
// console.log("Check: ", check(5));
// console.log("Test: ", test());

// import sqr, { check as chk, test as tst } from "./lib";
// console.log("Square: ", sqr(5));
// console.log("Check: ", chk(5));
// console.log("Test: ", tst());

import * as l from "./lib";
console.log("Square: ", l.default(5));
console.log("Check: ", l.check(5));
console.log("Test: ", l.test());