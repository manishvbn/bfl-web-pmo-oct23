'use strict'

// var count = 0;

// function next() {
//     return count += 1;
// }

// setInterval(() => {
//     console.log(next());
// }, 2000);

// setTimeout(() => {
//     count = "abc";
// }, 5000);

// -----------------------------

// function next() {
//     var count = 0;
//     return count += 1;
// }

// setInterval(() => {
//     console.log(next());
// }, 2000);

// -----------------------------

// Closure is a function that has access to the parent scope, 
// even after the scope has closed.

const next = (function () {
    var count = 0;

    return function () {
        return count += 1;
    }
})();

setInterval(() => {
    console.log(next());
}, 2000);