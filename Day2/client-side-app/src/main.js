// Import an entire module, for side effects only, without importing anything from the file.
// This will run the module's global code, but doesn't import any values.
import './1_datatypes/1_declarations';