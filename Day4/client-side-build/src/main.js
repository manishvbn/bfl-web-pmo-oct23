// Import an entire module, for side effects only, without importing anything from the file.
// This will run the module's global code, but doesn't import any values.

// import './1_objects/1_object-type';
// import './1_objects/2_custom-type';
// import './1_objects/3_es6-class';
// import './1_objects/4_es6-properties';
// import './1_objects/5_assignment';
// import './1_objects/6_inheritance';

// import './2_modules/usage';

import './3_ajax/dome-handler';