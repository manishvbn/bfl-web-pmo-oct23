import log from './common';

var cart = [];

function addToCart(item) {
    cart.push(item);
    log("added: " + item);
}

export default addToCart;