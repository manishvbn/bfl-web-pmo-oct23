// Syntax One - Default Export
// export default function square(x) {
//     return x * x;
// }

// Syntax Two - Named Export
// export function square(x) {
//     return x * x;
// }

// Syntax Three - Multi Export
// Only one default export allowed per module.
export default function square(x) {
    return x * x;
}

export function check(x) {
    return `Checked: ${x}`;
}

export function test(x) {
    return `Tested: ${x}`;
}