// let toy1 = new Object();
// console.log(toy1);
// console.log(typeof toy1);

// let toy2 = {};
// console.log(toy2);
// console.log(typeof toy2);

// let toy3 = null;
// console.log(toy3);
// console.log(typeof toy3);

// console.log(toy1.constructor);
// console.log(toy1.toString());

// toy1.color = "blue";
// toy1.shape = "circle";

// console.log(toy1);

// --------------------------------------------
let toy1 = new Object();
let toy2 = new Object();

// toy1.color = "blue";
// toy1.shape = "circle";

Object.prototype.color = "red";
Object.prototype.shape = "circle";

console.log("Toy 1 Color", toy1.color);
console.log("Toy 1 Shape", toy1.shape);

console.log("Toy 2 Color", toy2.color);
console.log("Toy 2 Shape", toy2.shape);

// Dunder Proto
// console.log(toy1.__proto__);
// console.log(toy2.__proto__);
// console.log(Object.prototype);

// console.log(toy1.__proto__ === toy2.__proto__);
// console.log(toy1.__proto__ === Object.prototype);
// console.log(toy2.__proto__ === Object.prototype);

// String.prototype.hello = function() {
//     console.log("Test");
// }

// let s = "Manish";
// s.hello();

toy2.color = "blue";
toy2.shape = "square";

console.log("Toy 1 Color", toy1.color);
console.log("Toy 1 Shape", toy1.shape);

console.log("Toy 2 Color", toy2.color);
console.log("Toy 2 Shape", toy2.shape);

console.log(toy1);
console.log(toy2);