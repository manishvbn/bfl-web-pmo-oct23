// 'use strict'

// // Function Creation
// // function <FunctionName>(<Paramaters>) {
// //     <Function Body>
// // }

// // Function Call
// // <FunctionName>(<Arguments>)

// function hello(person_name) {
//     // Template String (Template Literal) (String Literal)
//     console.log(`Hello, ${person_name}`);
// }

// hello("Manish");
// hello(10);
// hello();
// hello("Manish", "Sharma");

// // var fname = "Manish";
// // var lname = "Sharma";

// // var message = "Hello,\n\t" + fname + " " + lname;
// // console.log(message);

// // var message1 = `Hello, 
// //     ${fname} ${lname}`;
// // console.log(message1);

// --------------------------------------- Handle Less number of Arguments & Type
// Function to add 2 numbers
// function add(x, y) {
//     // if (x === void 0) x = 0;
//     // if (y === void 0) y = 0;

//     x = x || 0;
//     y = y || 0;

//     if ((typeof x !== 'number') || (typeof y !== 'number')) {
//         throw new Error('Arguments must be numbers');
//     }
//     else {
//         return x + y;
//     }
// }

// // ES 2015 - Default Parameters
// function add(x = 0, y = 0) {
//     if ((typeof x !== 'number') || (typeof y !== 'number')) {
//         throw new Error('Arguments must be numbers');
//     }
//     else {
//         return x + y;
//     }
// }

// console.log(add(20, 30));
// console.log(add(20));
// console.log(add());

// try {
//     console.log(add(10, 'abc'));
// } catch (e) {
//     console.error(e.message);
// }

// --------------------------------------- Handle More number of Arguments

// function hello(person_name) {
//     console.log(`Hello, ${person_name}`);
//     console.log(arguments);
// }

// hello("Manish");
// hello("Manish", "Sharma");
// hello("Manish", "Sharma", "Pune");
// hello("Manish", "Sharma", "Pune", 411021);

// console.log(hello);

// ---------------------------------------- REST parameter

function hello(person_name, ...extra) {
    console.log(`Hello, ${person_name}`);
    console.log(extra);
}

hello("Manish");
hello("Manish", "Sharma");
hello("Manish", "Sharma", "Pune");
hello("Manish", "Sharma", "Pune", 411021);
