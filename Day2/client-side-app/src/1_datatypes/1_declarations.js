// 'use strict'
// console.log('Hello from declarations.js file');

// // Global Scope
// // File Scope (Module)
// // Function Scope (Local)
// // Block Scope (only supported if you use let and const)

// // a = 10;
// // console.log('a is:', a);

// // ---------------------------

// // var a = 10;
// // console.log('a is:', a);

// // var b;
// // b = 20;
// // console.log('b is:', b);

// // var c = Number.MAX_SAFE_INTEGER;
// // console.log('c is:', c);
// // console.log('typeof c is:', typeof c);

// // c = "Manish";
// // console.log('c is:', c);
// // console.log('typeof c is:', typeof c);

// // var d = 10;
// // var d = "Manish";
// // console.log('d is:', d);

// // ----------------------------------------- Hoisting
// // Hoisting - Hoisting is JavaScript Runtime's default behavior of moving declarations to the top before execution

// // a = 10;
// // console.log(a);
// // var a;

// // console.log(b);
// // var b = 20;

// // --------------------
// // Function Scope (Local Scope)
// // Block Scoping is not supported with var keyword

// // var a = 10;

// // function test() {
// //     var a = 100;
// //     console.log("Inside Fn, a is:", a);
// // }

// // test();
// // console.log("Outside Fn, a is:", a);

// var i = 10;

// console.log("Before Loop, i is:", i);

// // for (var i = 0; i < 5; i++) {
// //     console.log("Inside Loop, i is:", i);
// // }

// // function iterate() {
// //     for (var i = 0; i < 5; i++) {
// //         console.log("Inside Loop, i is:", i);
// //     }
// // }

// // iterate();

// // IIFE (Immediatly Invoked Function Expression)

// (function () {
//     for (var i = 0; i < 5; i++) {
//         console.log("Inside Loop, i is:", i);
//     }
// })();

// console.log("After Loop, i is:", i);

// ----------------------------------------------------

// Not Hoisted
// i = 10;
// console.log(i);
// let i;

// let a = 10;
// let a = 20;

// let i = 10;

// console.log("Before Loop, i is:", i);

// for (let i = 0; i < 5; i++) {
//     console.log("Inside Loop, i is:", i);
// }

// console.log("After Loop, i is:", i);

// let a = 10;
// console.log(a);
// console.log(typeof a);

// let a;
// // a = 0b1001;
// // a = 0o1001;
// // a = 0x1001;
// console.log(a);
// console.log(typeof a);

// console.log(2 + parseInt("10"));
// console.log(2 + parseFloat("10.5"));
// console.log(2 + Number("10.5"));

// console.log(2 + parseInt("10asasas"));
// console.log(2 + parseFloat("10.5asdasd"));
// console.log(2 + Number("10.5asdasd"));

// console.log(2 + parseInt("asasas10"));
// console.log(2 + parseFloat("10.5asdasd"));
// console.log(2 + Number("10.5asdasd"));

var person = {
    id:1,
    name: "Manish",
    display: function() {
        console.log("Hello");
    }
};

// console.log(person);
// console.log(person.toString());

// var personJSON = JSON.stringify(person);

// var obj = JSON.parse(personJSON);

// localStorage.setItem("n", obj.name);
document.getElementById("hOne").innerText = localStorage.getItem("n");


// console.log(person);
// console.log(typeof person);

// let personJSON = JSON.stringify(person);

// console.log(personJSON);
// console.log(typeof personJSON);

// console.log(person.id);
// console.log(personJSON.id);

// console.log(person["id"]);
// console.log(personJSON["id"]);