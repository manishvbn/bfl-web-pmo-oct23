// Import an entire module, for side effects only, without importing anything from the file.
// This will run the module's global code, but doesn't import any values.

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import './css/main.css';

import 'bootstrap/dist/js/bootstrap.bundle.js';