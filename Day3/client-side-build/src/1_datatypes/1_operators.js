// We cannot specify the type of variable we are creating
// We can provide/reinitialize any value to any variable
// We can use any operator with any operand type

// var result = 10 * true;
// console.log(result);

// console.log(true && "abc");

// T && T = T
// T && F = F

{/* <h1 class={{isSelected() && "text-primary || "text-success"}}>Hello</h1> */ }

// var obj = { id: 1 };
// var obj;

// // if ((obj == null) || (obj == undefined)) {
// //     console.error("Object is not defined");
// // } else {
// //     console.log("object is: ", obj);
// // }

// if (!obj) {
//     console.error("Object is not defined");
// } else {
//     console.log("object is: ", obj);
// }

// let a = 10;
// let b = "10";

// console.log(typeof a);
// console.log(typeof b);

// console.log(a == b);
// console.log(a === b);

let a = { id: 1 };
let b = { id: 1 };

console.log(a == b);
console.log(a === b);

let c = b;          // Reference Copy

console.log(b == c);
console.log(b === c);
