// // Function Declaration Syntax
// function hello1() {
//     console.log("Hello One executed...");
// }

// // Function Expression Syntax
// const hello2 = function () {
//     console.log("Hello Two executed...");
// }

// // Function Constructor Syntax
// const hello3 = new Function("console.log('Hello Three executed...');");

// // Arrow Function Syntax (ES 2015)
// let hello4 = () => { 
//     console.log("Hello Four executed...");
// }

// hello1();
// hello2();
// hello3();
// hello4();

// -------------------------------------------------------

var i = 10;
console.log("i is: ", i);
console.log("Type of i is: ", typeof i);

var f = function () {
    console.log("Hello");
};
console.log("f is: ", f);
console.log("Type of f is: ", typeof f);

// Function is a type, which can refer to a block of code (Function Pointers/Delegates)

// Can we create a variable of type number?
// If yes; we should be able to create a variable of type function also

// Can we create a variable of type number inside a function?
// If yes; we should be able to create a variable of type function inside a function also (Nested Function)

// function a() {
//     function b() {
//         console.log("b executed...");
//     }
// }

// Can we return a variable of type number from a function?
// If yes; we should be able to return a variable of type function from a function also (Closure, Fn Currying, HOF)

// function a() {
//     function b() {
//         console.log("b executed...");
//     }

//     function c() {
//         console.log("b executed...");
//     }

//     return b;
// }

// var b = a();
// b();

// Can we pass a variable of type number to a function?
// If yes; we should be able to pass a variable of type function to a function also (Callback)

document.getElementById("btn").addEventListener("click", function () {
    console.log("Button Clicked...");
});

setInterval(function () {
    console.log("Interval executed...");
}, 1000);