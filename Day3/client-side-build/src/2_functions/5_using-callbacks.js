// // Dev 1
// function getString() {
//     const strArr = ["NodeJS", "ReactJS", "Angular", "ExtJS", "jQuery"];
//     const randomIndex = Math.floor(Math.random() * strArr.length);
//     const str = strArr[randomIndex];
//     return str;
// }

// // Dev 2
// // var s = getString();
// // console.log(s);

// setInterval(function () {
//     var s = getString();
//     console.log(s);
// }, 2000);

// // Call 1 - 1000
// // Call 2 - 3000
// // Call 3 - 1000

// --------------------------------------------
// // Dev 1
// function pushString(cb) {
//     // Async Code to call API
//     var sInt = setInterval(function () {
//         const strArr = ["NodeJS", "ReactJS", "Angular", "ExtJS", "jQuery"];
//         const randomIndex = Math.floor(Math.random() * strArr.length);
//         const str = strArr[randomIndex];
//         cb(str);
//     }, 2000);

//     setTimeout(function () {
//         clearInterval(sInt);
//     }, 10000);
// }

// // Dev 2
// pushString(function (s) {
//     console.log("S1: ", s);
// });

// pushString(function (s) {
//     console.log("S2: ", s);
// });

// -------------------------------------------- Promise Based
// Dev 1
function pushString() {
    return new Promise(function (resolve, reject) {
        // Async Code to call API
        setInterval(function () {
            const strArr = ["NodeJS", "ReactJS", "Angular", "ExtJS", "jQuery"];
            const randomIndex = Math.floor(Math.random() * strArr.length);
            const str = strArr[randomIndex];
            resolve(str);
        }, 2000);
    });
}

// Dev 2
var p = pushString();
// p.then(function (s) {
//     console.log("S1: ", s);
// }, function (err) {
//     console.error(err);
// });

p.then(function (s) {
    console.log("S1: ", s);
}).catch(function (err) {
    console.error(err);
});