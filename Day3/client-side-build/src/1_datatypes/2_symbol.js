// const color = "red";

// // Create a function, which should compare and give true, only if, 
// // color const is passed to it as an argument

// function compareColor(arg) {
//     console.log(arg === color);
// }

// compareColor(color);
// compareColor("red");

// var clr = "red";
// compareColor(clr);

// // ------------------------------------------- Reference Type
// const color = { code: "red" };

// // Create a function, which should compare and give true, only if, 
// // color const is passed to it as an argument

// function compareColor(arg) {
//     console.log(arg === color);
// }

// compareColor(color);
// compareColor({ code: "red" });

// var clr = { code: "red" };
// compareColor(clr);

// // ------------------------------------------- Symbol Type
// const color = Symbol("red");

// // Create a function, which should compare and give true, only if, 
// // color const is passed to it as an argument

// function compareColor(arg) {
//     console.log(arg === color);
// }

// compareColor(color);
// compareColor(Symbol("red"));

// var clr = Symbol("red");
// compareColor(clr);

// ----------------------------------------------------

// let person = { id: 1, name: "Manish", username: "ManishS", password: "123456" };

const password = Symbol("password");
let person = { id: 1, name: "Manish", username: "ManishS", [password]: "123456" };

console.log(person);
console.log(person[password]);

console.log(JSON.stringify(person));