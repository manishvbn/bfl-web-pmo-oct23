var employees = [
    { id: 1, name: "Manish", city: "Pune" },
    { id: 2, name: "Neeraj", city: "Delhi" },
    { id: 3, name: "Abhijeet", city: "Pune" }
];

// --------------------------------

// var pune_employees = [];

// for(var i = 0; i < employees.length; i++) {
//     if(employees[i].city === "Pune") {
//         pune_employees.push(employees[i]);
//     }
// }

// console.log(pune_employees);

// --------------------------------

// var pune_employees = [];

// function filterLogic(employee) {
//     return employee.city === "Pune";
// }

// for (var i = 0; i < employees.length; i++) {
//     if (filterLogic(employees[i])) {
//         pune_employees.push(employees[i]);
//     }
// }

// console.log(pune_employees);

// --------------------------------

// function filterLogic(employee) {
//     return employee.city === "Pune";
// }

// var pune_employees = employees.filter(filterLogic);

// console.log(pune_employees);

// --------------------------------

// var pune_employees = employees.filter(function (employee) {
//     return employee.city === "Pune";
// });

// console.log(pune_employees);

// -------------------------------- Multiline Arrow

// var pune_employees = employees.filter((employee) => {
//     return employee.city === "Pune";
// });

// console.log(pune_employees);

// -------------------------------- Singleline Arrow

var pune_employees = employees.filter(employee => employee.city === "Pune");
console.log(pune_employees);