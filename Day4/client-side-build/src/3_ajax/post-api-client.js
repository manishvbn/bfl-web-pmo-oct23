const url = 'https://jsonplaceholder.typicode.com/posts';

const postApiClient = {
    getAllPostsUsingCallbacks: function (successCB, errorCB) {
        fetch(url).then(response => {
            response.json().then(data => {
                successCB(data);
            }).catch(err => {
                errorCB("Error in JSON parsing...");
            })
        }).catch(err => {
            errorCB("Communication Error...");
        });
    },

    getAllPostsUsingPromise: function () {
        return new Promise((resolve, reject) => {
            fetch(url).then(response => {
                response.json().then(data => {
                    resolve(data);
                }).catch(err => {
                    reject("Error in JSON parsing...");
                })
            }).catch(err => {
                reject("Communication Error...");
            });
        });
    }
};

export default postApiClient;