// Import an entire module, for side effects only, without importing anything from the file.
// This will run the module's global code, but doesn't import any values.

// import './1_datatypes/1_operators';
// import './1_datatypes/2_symbol';

// import './2_functions/1_fn_creation';
// import './2_functions/2_fn_parameters';
// import './2_functions/3_pure-impure';
// import './2_functions/4_fn-overloading';
// import './2_functions/5_using-callbacks';
// import './2_functions/6_fn-as-arguments';
// import './2_functions/7_closure';
// import './2_functions/8_fn-currying';
import './2_functions/9_hof';
