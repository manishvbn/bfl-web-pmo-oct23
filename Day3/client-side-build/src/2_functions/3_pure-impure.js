'use strict'
var a = [10, 20, 30];

// Create a function, which can append item to an array

// function append(dataArr, item) {
//     dataArr.push(item);
//     return dataArr;
// }

function append(dataArr, item) {
    let tArr = [...dataArr];
    tArr.push(item);
    return tArr;
}

var newArr1 = append(a, 40);
console.log(newArr1);       // [10, 20, 30, 40]

var newArr2 = append(a, 40);
console.log(newArr2);       // [10, 20, 30, 40]