import postApiClient from "./post-api-client";

const ajaxDiv = document.querySelector('#ajaxDiv');
const messageDiv = document.querySelector('#messageDiv');

if (ajaxDiv.style.display === 'none') {
    ajaxDiv.style.display = 'block';
    messageDiv.style.display = 'none';
}

const button = document.createElement('button');
button.className = 'btn btn-primary';
button.innerHTML = 'Get Data';

var btnArea = document.querySelector('#btnArea');
btnArea.appendChild(button);

// 1_ Hard Coded Data
// button.addEventListener('click', function () {
//     // alert("Hello");

//     const data = [
//         {
//             "userId": 1,
//             "id": 1,
//             "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
//             "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
//         },
//         {
//             "userId": 1,
//             "id": 2,
//             "title": "qui est esse",
//             "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
//         }
//     ];

//     const table = generateTable(data);
//     const tableArea = document.querySelector('#tableArea');
//     tableArea.appendChild(table);
// });

// // 2_ Using Callbacks
// button.addEventListener('click', function () {
//     postApiClient.getAllPostsUsingCallbacks(function (data) {
//         const table = generateTable(data);
//         const tableArea = document.querySelector('#tableArea');
//         tableArea.appendChild(table);
//     }, function (err) {
//         console.error(err);
//     });
// });

// // 3_ Using Promise
// button.addEventListener('click', function () {
//     var p = postApiClient.getAllPostsUsingPromise();
//     p.then(function (data) {
//         const table = generateTable(data);
//         const tableArea = document.querySelector('#tableArea');
//         tableArea.appendChild(table);
//     }).catch(function (err) {
//         console.error(err);
//     });
// });

// 3_ Using Async Await
button.addEventListener('click', async function () {
    try {
        var data = await postApiClient.getAllPostsUsingPromise();
        const table = generateTable(data);
        const tableArea = document.querySelector('#tableArea');
        tableArea.appendChild(table);
    } catch (err) {
        console.error(err);
    }
});


function generateTable(data) {
    const table = document.createElement('table');
    table.className = 'table table-bordered table-striped';
    const headers = Object.keys(data[0]);

    const headerRow = document.createElement('tr');
    headers.forEach(headerText => {
        const header = document.createElement('th');
        const textNode = document.createTextNode(headerText.toUpperCase());
        header.appendChild(textNode);
        headerRow.appendChild(header);
    });
    table.appendChild(headerRow);

    data.forEach(obj => {
        const row = document.createElement('tr');

        headers.forEach(header => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(obj[header]);
            cell.appendChild(textNode);
            row.appendChild(cell);
        });
        table.appendChild(row);
    });

    return table;
}