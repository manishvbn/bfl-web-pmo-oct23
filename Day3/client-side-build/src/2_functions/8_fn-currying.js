// function greetings(message, name) {
//     console.log(`${message}, ${name}`);
// }

// greetings('Good Morning', 'Manish');
// greetings('Good Morning', 'Abhijeet');
// greetings('Good Morning', 'Ramakant');

// function Converter(toUnit, factor, offset, input) {
//     return [((offset + input) * factor).toFixed(2), toUnit].join("");
// }

// console.log(Converter(" INR", 83, 0, 100));
// console.log(Converter(" INR", 83, 0, 299));
// console.log(Converter(" INR", 83, 0, 876));
// console.log(Converter(" INR", 83, 0, 999));

// console.log(Converter(" KM", 1.6, 0, 100));
// console.log(Converter(" KM", 1.6, 0, 299));
// console.log(Converter(" KM", 1.6, 0, 876));
// console.log(Converter(" KM", 1.6, 0, 999));

// --------------------------------------

// function greetings(message) {
//     return function(name) {
//         console.log(`${message}, ${name}`);
//     }
// }

// const mGreet = greetings('Good Morning');

// mGreet('Manish');
// mGreet('Abhijeet');
// mGreet('Ramakant');

// const aGreet = greetings('Good Afternoon');

// aGreet('Manish');
// aGreet('Abhijeet');
// aGreet('Ramakant');

function Converter(toUnit, factor, offset) {
    return function(input) {
        return [((offset + input) * factor).toFixed(2), toUnit].join("");
    }
}

const usdToInrConverter = Converter(" INR", 83, 0);
console.log(usdToInrConverter(100));
console.log(usdToInrConverter(299));
console.log(usdToInrConverter(876));
console.log(usdToInrConverter(999));

const milesToKmConverter = Converter(" KM", 1.6, 0);
console.log(milesToKmConverter(100));
console.log(milesToKmConverter(299));
console.log(milesToKmConverter(876));
console.log(milesToKmConverter(999));