class Person {
    constructor(name, age) {
        // Data Property
        this._name = name;
        this._age = age;
    }

    // Accessor Properties
    get Name() {
        return this._name;
    }

    set Name(value) {
        this._name = value;
    }

    get Age() {
        return this._age;
    }

    set Age(value) {
        this._age = value;
    }
}

var p1 = new Person("Manish",0);
console.log(p1.Name);
console.log(p1.Age);
p1.Name = "Abhijeet";
p1.Age = 30;
console.log(p1.Name);
console.log(p1.Age);
